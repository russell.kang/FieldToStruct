package unkang

import (
	"fmt"
	"reflect"
	"strings"
)

/**
 * FieldToTruct 数据库表字段与结构对应， sql查询结果为struct
 * @param {[type]} st interface{}) (string, []interface{} [description]
 */
func FieldToStruct(st interface{}) (string, []interface{}) {
	rStruct := reflect.ValueOf(st).Elem()
	rType := rStruct.Type()

	values := []interface{}{}
	fields := []string{}

	for i, rlen := 0, rType.NumField(); i < rlen; i++ {
		if rType.Kind() == reflect.Struct {
			field := rType.Field(i).Tag.Get("sql")
			if field != "-" {
				fields = append(fields, field)
				values = append(values, rStruct.FieldByName(rType.Field(i).Name).Addr().Interface())
			}
		}
	}

	return FieldStr(fields), values
}

func FieldStr(fstrs []string) string {
	var result []string
	for _, fstr := range fstrs {
		if strings.Index(fstr, " as ") != -1 {
			result = append(result, strings.Replace(fstr, " as ", " as `", -1) + "`")
		} else {
			result = append(result, fmt.Sprintf("`%s`", fstr))
		}
	}

	return strings.Join(result, ",")
}
